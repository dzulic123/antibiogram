using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StapicAnimacije : MonoBehaviour
{
    Animator a;
    AntibiogramBehaviour antibiogram;
    public GameObject player;
    public GameObject epruveta;
    private EpruvetaAnimacija epruvetaSkripta;
    public GameObject bodovi;
    public Text scoreText;

    // Start is called before the first frame update
    void Start()
    {
        a = this.GetComponent<Animator>();
        antibiogram = player.GetComponent<AntibiogramBehaviour>();
        epruvetaSkripta = epruveta.GetComponent<EpruvetaAnimacija>();
    }

    // Update is called once per frame
    void OnMouseOver()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            int korak =antibiogram.DotaknutStapic();
            if (korak == 1)
            {
                a.enabled = true;
                a.Play("SterilniStapic1");
                bodovi.GetComponent<EnemyController>().bodovi += 100;
                scoreText.text = "Bodovi: " + (bodovi.GetComponent<EnemyController>().bodovi).ToString();
            }    
            else if(korak == 3)
            {
                a.Play("SterilniStapicUEpruvetu");
                bodovi.GetComponent<EnemyController>().bodovi += 100;
                scoreText.text = "Bodovi: " + (bodovi.GetComponent<EnemyController>().bodovi).ToString();
            }
            else if (korak == 4)
            {
                a.Play("SterilniStapicPetrijeva");
                epruvetaSkripta.SpremiEpruvetu();
            }

        }
    }
}
