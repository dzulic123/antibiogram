using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BocicaAnimacija : MonoBehaviour
{
    Animator a;
    AntibiogramBehaviour antibiogram;
    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        a = this.GetComponent<Animator>();
        antibiogram = player.GetComponent<AntibiogramBehaviour>();
    }

    // Update is called once per frame
    void OnMouseOver()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            int korak = antibiogram.DotaknutaBocica1();
            if (korak == 5)
            {
                a.enabled = true;
                a.Play("Pilula1");
            }
            if (korak == 8)
            {
                a.Play("BocicaPovratak2");
            }

        }
    }
}
