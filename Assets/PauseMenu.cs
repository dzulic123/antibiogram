using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;
    public GameObject kamera,player;

    public GameObject pauseMenuUI, control,volumemenu, audiomanager;
    private bool ulazak = false;
    private bool provjera=false;
    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape) && ulazak==false)
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
        if (!control.active && !volumemenu.active)
        {
            ulazak = false;
        }
        
        /*if (control.active && Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = 1f;
            Cursor.lockState = CursorLockMode.Locked;
            kamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = true;
            player.GetComponent<Igrac>().check = true;
            control.SetActive(false);
            ulazak = false;
        }*/

    }

    public void Resume()
    {
        Time.timeScale = 1f;
        Cursor.lockState = CursorLockMode.Locked;
        kamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = true;
        player.GetComponent<Igrac>().check = true;
        pauseMenuUI.SetActive(false);
        GameIsPaused = false;
    }

    void Pause()
    {
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0f;
        kamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
        player.GetComponent<Igrac>().check = false;
        pauseMenuUI.SetActive(true);
        GameIsPaused = true;
        //provjera = true;
    }
    public void kontrole()
    {
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0f;
        kamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
        player.GetComponent<Igrac>().check = false;
        pauseMenuUI.SetActive(false);
        control.SetActive(true);
        ulazak = true;
    }
    public void nazad()
    {
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0f;
        kamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
        player.GetComponent<Igrac>().check = false;
        pauseMenuUI.SetActive(true);
        control.SetActive(false);
        ulazak = false;
    }
    public void nazadizvolume()
    {
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0f;
        kamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
        player.GetComponent<Igrac>().check = false;
        pauseMenuUI.SetActive(true);
        volumemenu.SetActive(false);
        ulazak = false;
    }
    public void kontrolezvuka()
    {
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0f;
        kamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
        player.GetComponent<Igrac>().check = false;
        pauseMenuUI.SetActive(false);
        volumemenu.SetActive(true);
        ulazak = true;
    }
    public void LoadMenu()
    {
        //SceneManager.UnloadSceneAsync(1);
        Destroy(audiomanager);
        SceneManager.LoadScene("MENU");
        /*SceneManager.UnloadSceneAsync(1);
        SceneManager.LoadSceneAsync(0, LoadSceneMode.Additive);*/
    }
    public void QuitGame()
    {
        Application.Quit();
    }

}
