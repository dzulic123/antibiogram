﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class QuizManager : MonoBehaviour
{
    public List<QuestionsAndAnswers> questionAndAnswer;
    // gumbi
    public GameObject[] options;
    public int currentQuestion;
    public GameObject QuizCanvas;
    public Text QuestionText;
    // panel koji prikazuje kviz
    public GameObject QuizPanel;
    // panel koji prikazuje bodove
    public GameObject GoPanel;
    // prikaz bodova
    public Text ScoreText;
    int totalQuestions = 0;
    public int score,score1;
    private int lastQA;
    public Text text2;
    public bool drugiKrug;
    public GameObject kamera;
    public GameObject player,enemy;
    public Text bodovi;
    public GameObject petrijeva;
    public int i = 0;


    public void StartQuiz()
    {
        QuizCanvas.SetActive(true);
        AllQuestions allQuestions = new AllQuestions();
        int count = 0;
        if (/*drugiKrug == false*/(player.GetComponent<AntibiogramBehaviour>().drugiKrug==false && i==0) || (player.GetComponent<AntibiogramBehaviour>().drugiKrug == true && i == 0))
        {
            /*foreach (QuestionsAndAnswers qa in allQuestions.questionAndAnswer)
            {
                count++;
                if (count == 6)
                {
                    lastQA = count - 1;
                    continue;
                }
                else
                {
                    questionAndAnswer.Add(qa);
                }
            }*/
            for (int i = 0; i < 5; i++)
            {
                questionAndAnswer.Add(allQuestions.questionAndAnswer[i]);
            }
        }
        if(player.GetComponent<AntibiogramBehaviour>().drugiKrug == true && i>=1 /*&& petrijeva.GetComponent<PetrijevaAnimacija>().i==2*/)
        {
            //questionAndAnswer.Clear();
            QuizPanel.SetActive(true);
            for (int i = 5; i < allQuestions.questionAndAnswer.Count; i++)
            {
                questionAndAnswer.Add(allQuestions.questionAndAnswer[i]);
            }
        }
        //Debug.Log("drugiKrug:" + drugiKrug);

        totalQuestions = questionAndAnswer.Count;
        generateQuestion();
        GoPanel.SetActive(false);

    }
    void GameOver()
    {
        QuizPanel.SetActive(false);
        GoPanel.SetActive(true);
        /*if (player.GetComponent<AntibiogramBehaviour>().drugiKrug == false)
        {
            ScoreText.text = score + "/" + totalQuestions;
        }*/
        /*if (player.GetComponent<AntibiogramBehaviour>().drugiKrug == true)
        {
            ScoreText.text = score1 + "/" + totalQuestions;
        }*/
        if ((player.GetComponent<AntibiogramBehaviour>().drugiKrug == false && i==0) || (player.GetComponent<AntibiogramBehaviour>().drugiKrug == true && i == 0))
        {
            if (score == 5 /*|| (score1 == 5 && player.GetComponent<AntibiogramBehaviour>().drugiKrug == true)*/)
            {
                text2.text = "Bravo. Odgovorio/la si na sva pitanja točno. Dobit ćeš maksimalan broj bodova.";
                enemy.GetComponent<EnemyController>().bodovi += 100;
                bodovi.text = "Bodovi: " + (enemy.GetComponent<EnemyController>().bodovi).ToString();
            }
            else if ((score >= 3 && score < 5) /*|| (score1 >= 3 && score1<5 && player.GetComponent<AntibiogramBehaviour>().drugiKrug == true)*/)
            {
                text2.text = "Bravo. Imao/la si samo jedan netočan odgovor. Svejedno ćeš dobiti bodove, ali 50% manje. ";
                enemy.GetComponent<EnemyController>().bodovi += 50;
                bodovi.text = "Bodovi: " + (enemy.GetComponent<EnemyController>().bodovi).ToString();
            }
            else
            {
                text2.text = "Nažalost imao/la si više od jednog netočnog odgovora. Bodovi ti neće biti dodijeljeni.";
            }
            ScoreText.text = score + "/" + totalQuestions;
        }
        if(/*player.GetComponent<AntibiogramBehaviour>().drugiKrug == true*/ player.GetComponent<AntibiogramBehaviour>().drugiKrug == true && i >= 1)
        {
            if (score1 == 5)
            {
                text2.text = "Bravo. Odgovorio/la si na sva pitanja točno. Dobit ćeš maksimalan broj bodova.";
                enemy.GetComponent<EnemyController>().bodovi += 100;
                bodovi.text = "Bodovi: " + (enemy.GetComponent<EnemyController>().bodovi).ToString();
            }
            else if (score1 >= 3 && score1 < 5 )
            {
                text2.text = "Bravo. Imao/la si samo jedan netočan odgovor. Svejedno ćeš dobiti bodove, ali 50% manje. ";
                enemy.GetComponent<EnemyController>().bodovi += 50;
                bodovi.text = "Bodovi: " + (enemy.GetComponent<EnemyController>().bodovi).ToString();
            }
            else
            {
                text2.text = "Nažalost imao/la si više od jednog netočnog odgovora. Bodovi ti neće biti dodijeljeni.";
            }
            ScoreText.text = score1 + "/" + totalQuestions;
        }
    }

    public void correct()
    {
        if ((player.GetComponent<AntibiogramBehaviour>().drugiKrug == false && i == 0) || (player.GetComponent<AntibiogramBehaviour>().drugiKrug == true && i == 0))
        {
            score += 1;

            int correctOption = questionAndAnswer[currentQuestion].correctAnswer;
        }
        if (player.GetComponent<AntibiogramBehaviour>().drugiKrug == true && i >= 1)
        {
            score1 += 1;
        }

    }

    public void next()
    {
        // micanje trenutnog pitanja
        questionAndAnswer.RemoveAt(currentQuestion);
        generateQuestion();

    }

    public void wrong()
    {
        int correctOption = questionAndAnswer[currentQuestion].correctAnswer;
    }

    public void Exit()
    {
        //Application.Quit();
        //Debug.Log("Izlaz");
        QuizCanvas.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        kamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = true;
        Time.timeScale = 1f;
        i += 1;
    }

    void setAnswers()
    {
        // petlja koja prolazi po gumbima
        for (int i = 0; i < options.Length; i++)
        {
            options[i].GetComponent<AnswerScript>().isCorrect = false;

            Color purple = new Color();
            ColorUtility.TryParseHtmlString("#380838", out purple);
            options[i].GetComponent<Image>().color = purple;

            // dohvaæa se tekst iz button-a i na njegovo mjesto se postavlja odgovor iz AllQuestion skripte
            options[i].transform.GetChild(0).GetComponent<Text>().text = questionAndAnswer[currentQuestion].Answers[i];
            // i je 0 pa zato ide i + 1 
            if (questionAndAnswer[currentQuestion].correctAnswer == i + 1)
            {
                // dohvat toènog odgovora
                options[i].GetComponent<AnswerScript>().isCorrect = true;
            }
        }
    }

    void generateQuestion()
    {
        if (questionAndAnswer.Count > 0)
        {
            // dohvaæanje random pitanja iz questionAndAnswer od 0 do broja pitanja
            currentQuestion = Random.Range(0, questionAndAnswer.Count);
            QuestionText.text = questionAndAnswer[currentQuestion].Question;
            setAnswers();

        }
        else
        {
            Debug.Log("Kviz je završen.");
            GameOver();
        }
    }
}
