using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bocica3Animacija : MonoBehaviour
{
    Animator a;
    AntibiogramBehaviour antibiogram;
    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        a = this.GetComponent<Animator>();
        antibiogram = player.GetComponent<AntibiogramBehaviour>();
    }

    // Update is called once per frame
    void OnMouseOver()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            int korak = antibiogram.DotaknutaBocica3();
            Debug.Log("bocica3:" + korak);
            if (korak ==12)
            {
                a.enabled = true;
                a.Play("Pilula3");
            }
            if (korak == 14)
            {
                a.Play("BocicaPovratak3");
            }

        }
    }
}
