using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InkubatorAnimacija : MonoBehaviour
{
    Animator a;
    AntibiogramBehaviour antibiogram;
    public GameObject player;
    public bool open;
    public bool drugiKrug;

    // Start is called before the first frame update
    void Start()
    {
        a = this.GetComponent<Animator>();
        antibiogram = player.GetComponent<AntibiogramBehaviour>();
    }
    void OnMouseOver()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            int korak = antibiogram.DotaknutInkubator();
            drugiKrug = antibiogram.drugiKrug;
            if (drugiKrug == false)
            {
                if (korak == 12 || korak == 15)
                {
                    if (player)
                    {
                        float dist = Vector3.Distance(player.transform.position, transform.position);
                        if (dist < 15)
                        {
                            if (open == false)
                            {
                                open = true;
                                a.Play("Opening 1");
                            }
                        }
                    }
                }
                else if (korak == 14 || korak == 17)
                {
                    if (player)
                    {
                        float dist = Vector3.Distance(player.transform.position, transform.position);
                        if (dist < 15)
                        {
                            if (open == true)
                            {
                                a.Play("Closing 1");
                                open = false;
                            }

                        }
                    }
                }
            }
            //drugi krug
            else
            {
                if (korak == 15 || korak == 18)
                {
                    if (player)
                    {
                        float dist = Vector3.Distance(player.transform.position, transform.position);
                        if (dist < 15)
                        {
                            if (open == false)
                            {
                                open = true;
                                a.Play("Opening 1");
                            }
                        }
                    }
                }
                else if (korak == 17 || korak == 20)
                {
                    if (player)
                    {
                        float dist = Vector3.Distance(player.transform.position, transform.position);
                        if (dist < 15)
                        {
                            if (open == true)
                            {
                                a.Play("Closing 1");
                                open = false;
                            }

                        }
                    }
                }
            }
           
           
        }
    }
}
