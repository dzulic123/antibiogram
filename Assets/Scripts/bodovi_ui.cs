using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bodovi_ui : MonoBehaviour
{
    public GameObject enemy,enemy1;
    private bool bodoviblue = false, zelena = false, zuta = false,color1=false, orange1=false, purple1=false,red1=false;
    private int bodovi;
    public void blue()
    {
        /*Material newMat = Resources.Load("DEV_Orange", typeof(Material)) as Material;
        gameObject.renderer.material = newMat;*/
        if (enemy.GetComponent<EnemyController>().bodovi >= 100)
        {
            Material[] mats = GetComponent<SkinnedMeshRenderer>().materials;
            Material mat = (Material)Resources.Load("Blue");
            mats[0] = mat;
            GetComponent<SkinnedMeshRenderer>().materials = mats;
        }
    }
    public void original()
    {
        /*Material newMat = Resources.Load("DEV_Orange", typeof(Material)) as Material;
        gameObject.renderer.material = newMat;*/
        Material[] mats = GetComponent<SkinnedMeshRenderer>().materials;
        Material mat = (Material)Resources.Load("Ch41_body");
        mats[0] = mat;
        GetComponent<SkinnedMeshRenderer>().materials = mats;
    }
    public void green()
    {
        /*Material newMat = Resources.Load("DEV_Orange", typeof(Material)) as Material;
        gameObject.renderer.material = newMat;*/
        if (enemy.GetComponent<EnemyController>().bodovi >= 200)
        {
            Material[] mats = GetComponent<SkinnedMeshRenderer>().materials;
            Material mat = (Material)Resources.Load("Green");
            mats[0] = mat;
            GetComponent<SkinnedMeshRenderer>().materials = mats;
            zelena = true;
        }
    }
    public void yellow()
    {
        /*Material newMat = Resources.Load("DEV_Orange", typeof(Material)) as Material;
        gameObject.renderer.material = newMat;*/
        if (enemy.GetComponent<EnemyController>().bodovi >= 300)
        {
            Material[] mats = GetComponent<SkinnedMeshRenderer>().materials;
            Material mat = (Material)Resources.Load("Yellow");
            mats[0] = mat;
            GetComponent<SkinnedMeshRenderer>().materials = mats;
            zuta = true;
        }
    }
    public void color()
    {
        /*Material newMat = Resources.Load("DEV_Orange", typeof(Material)) as Material;
        gameObject.renderer.material = newMat;*/
        if (enemy.GetComponent<EnemyController>().bodovi >= 400)
        {
            Material[] mats = GetComponent<SkinnedMeshRenderer>().materials;
            Material mat = (Material)Resources.Load("Color");
            mats[0] = mat;
            GetComponent<SkinnedMeshRenderer>().materials = mats;
            color1 = true;
        }
    }
    public void orange()
    {
        /*Material newMat = Resources.Load("DEV_Orange", typeof(Material)) as Material;
        gameObject.renderer.material = newMat;*/
        if (enemy.GetComponent<EnemyController>().bodovi >= 500)
        {
            Material[] mats = GetComponent<SkinnedMeshRenderer>().materials;
            Material mat = (Material)Resources.Load("Orange");
            mats[0] = mat;
            GetComponent<SkinnedMeshRenderer>().materials = mats;
            orange1 = true;
        }
    }
    public void purple()
    {
        /*Material newMat = Resources.Load("DEV_Orange", typeof(Material)) as Material;
        gameObject.renderer.material = newMat;*/
        if (enemy.GetComponent<EnemyController>().bodovi >= 600)
        {
            Material[] mats = GetComponent<SkinnedMeshRenderer>().materials;
            Material mat = (Material)Resources.Load("Purple");
            mats[0] = mat;
            GetComponent<SkinnedMeshRenderer>().materials = mats;
            purple1 = true;
            //red1 = true;
        }
    }
    public void red()
    {
        /*Material newMat = Resources.Load("DEV_Orange", typeof(Material)) as Material;
        gameObject.renderer.material = newMat;*/
        if (enemy.GetComponent<EnemyController>().bodovi >= 800)
        {
            Material[] mats = GetComponent<SkinnedMeshRenderer>().materials;
            Material mat = (Material)Resources.Load("Red");
            mats[0] = mat;
            GetComponent<SkinnedMeshRenderer>().materials = mats;
            red1 = true;
        }
    }
    /*void Update()
    {
        if (enemy.GetComponent<EnemyController>().bodovi >= 0)
        {
            bodoviblue = true;
        }
    }*/
    void Update()
    {
        bodovi = enemy.GetComponent<EnemyController>().bodovi;
        if (bodovi < 100)
        {
            Material[] mats = GetComponent<SkinnedMeshRenderer>().materials;
            Material mat = (Material)Resources.Load("Ch41_body");
            mats[0] = mat;
            GetComponent<SkinnedMeshRenderer>().materials = mats;
        }
        if (bodovi >= 100 && bodovi < 200 && zelena ==true)
        {
            Material[] mats = GetComponent<SkinnedMeshRenderer>().materials;
            Material mat = (Material)Resources.Load("Blue");
            mats[0] = mat;
            GetComponent<SkinnedMeshRenderer>().materials = mats;
            zelena = false;
        }
        if (bodovi >= 200 && bodovi < 300 && zuta == true)
        {
            Material[] mats = GetComponent<SkinnedMeshRenderer>().materials;
            Material mat = (Material)Resources.Load("Green");
            mats[0] = mat;
            GetComponent<SkinnedMeshRenderer>().materials = mats;
            zuta = false;
        }
        if (bodovi >= 300 && bodovi < 400 && color1 == true)
        {
            Material[] mats = GetComponent<SkinnedMeshRenderer>().materials;
            Material mat = (Material)Resources.Load("Yellow");
            mats[0] = mat;
            GetComponent<SkinnedMeshRenderer>().materials = mats;
            color1 = false;
        }
        if (bodovi >= 400 && bodovi < 500 && orange1 == true )
        {
            Material[] mats = GetComponent<SkinnedMeshRenderer>().materials;
            Material mat = (Material)Resources.Load("Color");
            mats[0] = mat;
            GetComponent<SkinnedMeshRenderer>().materials = mats;
            orange1 = false;
        }
        if (bodovi >= 500 && bodovi < 600 && purple1 == true)
        {
            Material[] mats = GetComponent<SkinnedMeshRenderer>().materials;
            Material mat = (Material)Resources.Load("Orange");
            mats[0] = mat;
            GetComponent<SkinnedMeshRenderer>().materials = mats;
            purple1 = false;
            //red1 = false;
        }
        if (bodovi >= 600 && bodovi < 800 && red1 == true)
        {
            Material[] mats = GetComponent<SkinnedMeshRenderer>().materials;
            Material mat = (Material)Resources.Load("Purple");
            mats[0] = mat;
            GetComponent<SkinnedMeshRenderer>().materials = mats;
            red1 = false;
        }
    }
}
