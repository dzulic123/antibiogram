using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

[RequireComponent(typeof(NavMeshAgent), typeof(Animator))]
public class EnemyController2 : MonoBehaviour
{
    private static readonly int SpeedFloat = Animator.StringToHash("Speed");
    private Animator anim;
    private NavMeshAgent navAgent;
    private Transform following;
    bool sjestinav1 = false;
    bool sjestinav2 = false;
    bool sjestinav = false;
    bool sjestinav3 = false;
    public GameObject stolica, stolica1, stolica2, stolica3,npc;
    private bool jednom = false;
    public GameObject bodovi;
    public Text scoreText;
    // Start is called before the first frame update
    void Start()
    {
        navAgent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        var velocity = navAgent.velocity.magnitude / navAgent.speed;
        anim.SetFloat(SpeedFloat, velocity);
        if (Input.GetKeyDown(KeyCode.Alpha3) && !anim.GetCurrentAnimatorStateInfo(0).IsName("Stand To Sit"))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                navAgent.SetDestination(hit.point);
            }
        }

        if (sjestinav2 == true && Input.GetKeyDown(KeyCode.R) && anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            navAgent.SetDestination(stolica.transform.position);
            navAgent.enabled = false;
            //npc.transform.Rotate(stolicapozicija);
            npc.transform.rotation = stolica.transform.rotation;
            anim.SetTrigger("sjedni");
        }
        if (sjestinav == true && Input.GetKeyDown(KeyCode.R) && anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            navAgent.SetDestination(stolica1.transform.position);
            navAgent.enabled = false;
            npc.transform.rotation = stolica1.transform.rotation;
            anim.SetTrigger("sjedni");
        }
        if (sjestinav1 == true && Input.GetKeyDown(KeyCode.R) && anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            navAgent.SetDestination(stolica2.transform.position);
            navAgent.enabled = false;
            npc.transform.rotation = stolica2.transform.rotation;
            anim.SetTrigger("sjedni");
        }
        if (sjestinav3 == true && Input.GetKeyDown(KeyCode.R) && anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            navAgent.SetDestination(stolica3.transform.position);
            navAgent.enabled = false;
            npc.transform.rotation = stolica3.transform.rotation;
            anim.SetTrigger("sjedni");
        }
        if ((Input.GetKeyDown(KeyCode.Alpha3) && anim.GetCurrentAnimatorStateInfo(0).IsName("Stand To Sit") && sjestinav2 == true) ||
            (Input.GetKeyDown(KeyCode.Alpha3) && anim.GetCurrentAnimatorStateInfo(0).IsName("Stand To Sit") && sjestinav == true) ||
            (Input.GetKeyDown(KeyCode.Alpha3) && anim.GetCurrentAnimatorStateInfo(0).IsName("Stand To Sit") && sjestinav1 == true) ||
            (Input.GetKeyDown(KeyCode.Alpha3) && anim.GetCurrentAnimatorStateInfo(0).IsName("Stand To Sit") && sjestinav3 == true))
        {
            navAgent.enabled = true;
            anim.SetTrigger("notsitting");
            anim.SetTrigger("standingidle");
        }

        /*if (sjestinav1 == true && Input.GetKeyDown(KeyCode.R) && anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            navAgent.enabled = false;
            anim.SetTrigger("sjedni");
        }
        if (Input.GetKeyDown(KeyCode.Alpha3) && anim.GetCurrentAnimatorStateInfo(0).IsName("Stand To Sit") && sjestinav1==true)
        {
            navAgent.enabled = true;
            anim.SetTrigger("notsitting");
            anim.SetTrigger("standingidle");
        }*/
        /*if (Input.GetKeyDown(KeyCode.R) && anim.GetCurrentAnimatorStateInfo(0).IsName("Sit To Stand") && sjestinav1 == true)
        {
            navAgent.enabled = false;
            anim.SetTrigger("sitagain");
        }
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Sit To Stand") && Input.GetKeyDown(KeyCode.Alpha3))
        {
            anim.SetTrigger("standingidle");
        }  */
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            following = other.gameObject.transform;
            Debug.Log("following:" + following);
        }
        if (other.CompareTag("sjestnavagent1"))
        {
            sjestinav2 = true;
        }
        if (other.CompareTag("sjestinavagent2"))
        {
            sjestinav = true;
        }
        if (other.CompareTag("sjestinavagent3"))
        {
            sjestinav1 = true;
        }
        if (other.CompareTag("sjestinavagent4"))
        {
            sjestinav3 = true;
        }
        if (other.CompareTag("object"))
        {
            if (!jednom)
            {
                bodovi.GetComponent<EnemyController>().bodovi += 100;
                scoreText.text = "Bodovi: " + (bodovi.GetComponent<EnemyController>().bodovi).ToString();
                jednom = true;
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            following = null;
        }
        if (other.CompareTag("sjestnavagent1"))
        {
            sjestinav2 = false;
        }
        if (other.CompareTag("sjestinavagent2"))
        {
            sjestinav = false;
        }
        if (other.CompareTag("sjestinavagent3"))
        {
            sjestinav1 = false;
        }
        if (other.CompareTag("sjestinavagent4"))
        {
            sjestinav3 = false;
        }
    }
}
