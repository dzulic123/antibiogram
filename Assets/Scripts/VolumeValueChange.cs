﻿using UnityEngine;

public class VolumeValueChange : MonoBehaviour {

    private AudioSource audioSrc, audioSrc1, audioSrc2, audioSrc3, audioSrc4, audioSrc5, audioSrc6;

    private float musicVolume = 1f;
    private float musicVolume1 = 1f;
    public AudioManager audiom;
    private int i = 0;

	void Start () {

        audioSrc = GetComponent<AudioSource>();
        audioSrc1 = GetComponent<AudioSource>();
        audioSrc2 = GetComponent<AudioSource>();
        audioSrc3 = GetComponent<AudioSource>();
        audioSrc4 = GetComponent<AudioSource>();
        audioSrc5 = GetComponent<AudioSource>();
        audioSrc6 = GetComponent<AudioSource>();
        foreach (var sound in audiom.GetComponent<AudioManager>().sounds)
        //for(int i=0;i<= 4;i++)
        {
            
            //audioSrc=audiom.GetComponent<AudioManager>().sounds[0];
            if (i == 0)
            {
                audioSrc = sound.source;
                //audioSrc.volume = sound.source.volume;
            }
            if (i == 1)
            {
                audioSrc1 = sound.source;
                //audioSrc1.volume = sound.source.volume;
            }
            if (i == 2)
            {
                audioSrc2 = sound.source;
                //audioSrc2.volume = sound.source.volume;
            }
            if (i == 3)
            {
                audioSrc3 = sound.source;
                //audioSrc3.volume = sound.source.volume;
            }
            if (i == 4)
            {
                audioSrc4 = sound.source;
                //audioSrc4.volume = sound.source.volume;
            }
            if (i == 5)
            {
                audioSrc5 = sound.source;
                //audioSrc4.volume = sound.source.volume;
            }
            if (i == 6)
            {
                audioSrc6 = sound.source;
                //audioSrc4.volume = sound.source.volume;
            }
            i += 1;
        }
    }
	
	// Update is called once per frame
	void Update () {

        audioSrc.volume = musicVolume;
        audioSrc1.volume = musicVolume;
        audioSrc2.volume = musicVolume;
        audioSrc3.volume = musicVolume;
        audioSrc4.volume = musicVolume;
        audioSrc5.volume = musicVolume;
        audioSrc6.volume = musicVolume1;
    }

    public void SetVolume(float vol)
    {
        musicVolume = vol;
    }
    public void SetVolumebackground(float vol)
    {
        musicVolume1 = vol;
    }
}
