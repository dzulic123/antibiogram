using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Igrac : MonoBehaviour
{
    private Animator anim;
    private static readonly int SpeedFloat = Animator.StringToHash("Speed");
    static readonly int punchTrigger = Animator.StringToHash("Punch");
    static readonly int idlepunchTrigger = Animator.StringToHash("idle_punch");
    static readonly int conditionTrigger = Animator.StringToHash("condition");
    static readonly int conditionState = Animator.StringToHash("condition");
    private Animation animation;
    [HideInInspector]
    public int counter = 0;
    private float timer = 0.0f;
    public GameObject napadni;
    public GameObject pomocnik;
    public GameObject collider;
    public GameObject character;
    public Texture2D crosshair;
    public GameObject quest1;
    public GameObject quest2;
    public GameObject quest3;
    //public GameObject quest4;//kviz
    public QuizManager quizGameObject;
    private bool jednom = false;
    private bool jednom1 = false;
    private bool inventory;
    public GameObject invertorygamobj, kamera1;
    public Text scoreText;
    private bool udaracinventory = false;
    public GameObject zdjelica, zdjelica1;
    public bool jednom_izvrsi=false, jednom_izvrsi1 = false;
    public GameObject quizcanvas,pausemenu,pausemenu1, pausemenu2;
    public bool check = true;
    private bool provjera = false;
    public GameObject inventoryui;
    //public GameObject kamera;

    void OnGUI()
    {
        if (check == true)
        {
            GUI.DrawTexture(new Rect(Screen.width / 2 - (crosshair.width * 0.5f),
                                        Screen.height / 2 - (crosshair.height * 0.5f),
                                             crosshair.width, crosshair.height), crosshair);
        }
    }
    void Awake()
    {
        collider.GetComponent<BoxCollider>().enabled = false;
    }
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        anim = GetComponent<Animator>();
        animation = GetComponent<Animation>();
        quest1.SetActive(true);
        quest2.SetActive(false);
        quest3.SetActive(false);
        StartCoroutine(Quest1Timer());
        //StartCoroutine(QuestQuizTimer());

    }

    // Update is called once per frame
    void Update()
    {
        var punch = Input.GetMouseButton(0);
        var notpunch = Input.GetMouseButtonUp(0);

        // Set the input axes on the Animator Controller.
        if (punch && !inventoryui.active && !pausemenu.active && !quizcanvas.active && !pausemenu1.active && !pausemenu2.active)
        {
            timer += Time.deltaTime;
                anim.SetInteger(conditionTrigger, 1);
        }
        if (notpunch && !inventoryui.active && !pausemenu.active && !quizcanvas.active && !pausemenu1.active && !pausemenu2.active)
        {
            timer = 0;
            anim.SetInteger(conditionTrigger, 0);
        }
        if(napadni.GetComponent<EnemyController>().napad == true && System.Math.Round(timer,1)==0.6 && punch)
        {
            //AudioManager.Instance.Pause(SoundType.BackgroundTheme);
            AudioManager.Instance.Play(SoundType.Punch);
        }
        if (napadni.GetComponent<EnemyController>().napad == true && System.Math.Round(timer, 1) == 2.1 && punch)
        {
            AudioManager.Instance.Play(SoundType.Punch);
        }
        if (napadni.GetComponent<EnemyController>().napad == true && System.Math.Round(timer, 1) == 3.8 && punch)
        {
            AudioManager.Instance.Play(SoundType.Punch);
        }
        if (napadni.GetComponent<EnemyController>().napad == true && System.Math.Round(timer, 1) == 5.3 && punch)
        {
            AudioManager.Instance.Play(SoundType.Punch);
        }
        if (napadni.GetComponent<EnemyController>().napad == true && System.Math.Round(timer, 1) == 6.8 && punch)
        {
            AudioManager.Instance.Play(SoundType.Punch);
        }
        if (timer >= 7.5f && napadni.GetComponent<EnemyController>().napad == true && !provjera)
        {
            counter = 5;
            AudioManager.Instance.Play(SoundType.Death);
            StartCoroutine(muzika());
            provjera = true;
        }
        /*if (System.Math.Round(timer, 1) == 8.0)
        {
            AudioManager.Instance.Play(SoundType.Death);
        }*/
        if (Input.GetMouseButtonDown(1))
        {
            anim.SetTrigger("dodging");
        }
        if (Input.GetMouseButtonUp(1))
        {
            anim.SetTrigger("notdodging");
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            inventory = !inventory;
        }
        if (inventory == true && !quizcanvas.active && !pausemenu.active)
        {
            //Debug.Log("prvi");
            Cursor.lockState = CursorLockMode.None;
            kamera1.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
            Time.timeScale = 0f;
            invertorygamobj.SetActive(true);
        }
        if(inventory==false && !quizcanvas.active && !pausemenu.active && !pausemenu1.active && !pausemenu2.active)
        {
            //Debug.Log("drugi");
            Cursor.lockState = CursorLockMode.Locked;
            kamera1.GetComponent<ThirdPersonOrbitCamBasic>().enabled = true;
            Time.timeScale = 1f;
            invertorygamobj.SetActive(false);
        }
        if (zdjelica.GetComponent<PetrijevaAnimacija>().ulazak == true && !jednom_izvrsi /*&& inventory==false*/)
        {
            //Debug.Log("treci");
            invertorygamobj.SetActive(false);
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = 0f;
            kamera1.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
            quizGameObject.StartQuiz();
            jednom_izvrsi = true;
        }
        if (zdjelica1.GetComponent<PetrijevaAnimacija>().ulazak1 == true && !jednom_izvrsi1 /*&& inventory == false*/)
        {
            //Debug.Log("cetvrti");
            invertorygamobj.SetActive(false);
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = 0f;
            kamera1.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
            quizGameObject.StartQuiz();
            jednom_izvrsi1 = true;
        }
        /*if(inventory==false && zdjelica.GetComponent<PetrijevaAnimacija>().ulazak == true && !jednom_izvrsi)
        {
            invertorygamobj.SetActive(false);
            Cursor.lockState = CursorLockMode.None;
            kamera1.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
            Time.timeScale = 0f;
            quizGameObject.StartQuiz();
            jednom_izvrsi = true;
        }*/
    }
    private IEnumerator Quest1Timer()
    {
        yield return new WaitForSeconds(10);
        //quest1.SetActive(false);
        Destroy(quest1);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("questulaz"))
        {
            if (!jednom)
            {
                napadni.GetComponent<EnemyController>().bodovi += 100;
                scoreText.text = "Bodovi: " + (napadni.GetComponent<EnemyController>().bodovi).ToString();
                quest2.SetActive(true);
                StartCoroutine(Quest2Timer());
                jednom = true;
            }
        }
        if (other.CompareTag("quest_antibiogram"))
        {
            if (/*quest2 != null &&*/ !jednom1)
                {
                //Destroy(quest1);
                    Destroy(quest2);
                    quest3.SetActive(true);
                    StartCoroutine(Quest3Timer());
                    jednom1 = true;
                }
        }
    }
    private IEnumerator Quest2Timer()
    {
        yield return new WaitForSeconds(10);
        Destroy(quest2);
    }
    private IEnumerator Quest3Timer()
    {
        yield return new WaitForSeconds(10);
        //quest3.SetActive(false);
        Destroy(quest3);
    }
    private IEnumerator QuestQuizTimer()
    {
        yield return new WaitForSeconds(1);
        //quest4.SetActive(false);
        quizGameObject.StartQuiz();
        //Destroy(quest4);
    }
    private IEnumerator muzika()
    {
        yield return new WaitForSeconds(2);
        AudioManager.Instance.Play(SoundType.BackgroundTheme);
    }
}
