﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Pipes;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager1 : MonoBehaviour
{
    public NPC npc;

    bool isTalking = false;

    float distance;
    float curResponseTracker = 0;

    public GameObject player;
    public GameObject dialogueUI;

    public Text npcName;
    public Text npcDialogueBox;
    public Text playerResponse;
    private Animator anim;



    // Start is called before the first frame update
    void Start()
    {
        dialogueUI.SetActive(false);
        anim = player.GetComponent<Animator>();
    }

    void OnMouseOver()
    {
        distance = Vector3.Distance(player.transform.position, this.transform.position);
        if(distance <= 2.5f)
        {
            if(Input.GetKeyDown(KeyCode.E) && isTalking==false)
            {
                player.GetComponent<MoveBehaviour>().enabled = false;
                anim.SetFloat("Speed", 0);
                StartConversation();
            }
            else if(Input.GetKeyDown(KeyCode.E) && isTalking == true)
            {
                player.GetComponent<MoveBehaviour>().enabled = true;
                EndDialogue();
            }
            
            if (curResponseTracker == 0 && npc.playerDialogue.Length >= 0)
            {
                playerResponse.text = npc.playerDialogue[0];
            }
        }
    }

    void StartConversation()
    {
        isTalking = true;
        curResponseTracker = 0;
        dialogueUI.SetActive(true);
        npcName.text = npc.name;
        npcDialogueBox.text = npc.dialogue[0];
    }

    void EndDialogue()
    {
        isTalking = false;
        dialogueUI.SetActive(false);
    }
    private IEnumerator ActivateOnTimer()
    {
        yield return new WaitForSeconds(10f);
        npcDialogueBox.text = npc.dialogue[1];

    }
}
