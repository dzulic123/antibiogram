using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

[RequireComponent(typeof(NavMeshAgent), typeof(Animator))]
public class lastenemy : MonoBehaviour
{
    private static readonly int SpeedFloat = Animator.StringToHash("Forward");
    private Animator anim;
    private NavMeshAgent navAgent;
    private Transform following;
    private float elapsed;
    private bool antibiotik = false;
    bool sjestinav2 = false;
    bool sjestinav = false;
    bool sjestinav1 = false;
    bool sjestinav3 = false;
    private Vector3 pozicija;
    public Transform teleportTarget;
    private bool jednom = false;
    public GameObject stolica, stolica1, stolica2, stolica3;
    public GameObject npc;
    private Vector3 stolicapozicija;
    public GameObject bodovi;
    public Text scoreText;
    void Start()
    {
        navAgent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        pozicija = new Vector3(teleportTarget.transform.position.x, (teleportTarget.transform.position.y + 0.55f), teleportTarget.transform.position.z);
        stolicapozicija = new Vector3(stolica.transform.position.x, stolica.transform.position.y, stolica.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        var velocity = navAgent.velocity.magnitude / navAgent.speed;
        anim.SetFloat(SpeedFloat, velocity);
        if (Input.GetKeyDown(KeyCode.Alpha4) && !anim.GetCurrentAnimatorStateInfo(0).IsName("Stand To Sit") && navAgent.enabled == true)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                navAgent.SetDestination(hit.point);
            }
        }
        /*if (sjestinav2 == true)
        {
            navAgent.SetDestination(stolica.transform.position);
        }*/
        if (sjestinav2 == true && Input.GetKeyDown(KeyCode.R) && anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            navAgent.SetDestination(stolica.transform.position);
            navAgent.enabled = false;
            //npc.transform.Rotate(stolicapozicija);
            npc.transform.rotation = stolica.transform.rotation;
            anim.SetTrigger("sjedni");
        }
        if (sjestinav == true && Input.GetKeyDown(KeyCode.R) && anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            navAgent.SetDestination(stolica1.transform.position);
            navAgent.enabled = false;
            npc.transform.rotation = stolica1.transform.rotation;
            anim.SetTrigger("sjedni");
        }
        if (sjestinav1 == true && Input.GetKeyDown(KeyCode.R) && anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            navAgent.SetDestination(stolica2.transform.position);
            navAgent.enabled = false;
            npc.transform.rotation = stolica2.transform.rotation;
            anim.SetTrigger("sjedni");
        }
        if (sjestinav3 == true && Input.GetKeyDown(KeyCode.R) && anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            navAgent.SetDestination(stolica3.transform.position);
            navAgent.enabled = false;
            npc.transform.rotation = stolica3.transform.rotation;
            anim.SetTrigger("sjedni");
        }
        if ((Input.GetKeyDown(KeyCode.Alpha4) && anim.GetCurrentAnimatorStateInfo(0).IsName("Stand To Sit") && sjestinav2 == true) ||
            (Input.GetKeyDown(KeyCode.Alpha4) && anim.GetCurrentAnimatorStateInfo(0).IsName("Stand To Sit") && sjestinav == true) ||
            (Input.GetKeyDown(KeyCode.Alpha4) && anim.GetCurrentAnimatorStateInfo(0).IsName("Stand To Sit") && sjestinav1 == true) ||
            (Input.GetKeyDown(KeyCode.Alpha4) && anim.GetCurrentAnimatorStateInfo(0).IsName("Stand To Sit") && sjestinav3 == true))
        {
            navAgent.enabled = true;
            anim.SetTrigger("notsitting");
            anim.SetTrigger("standingidle");
        }
        /*if (Input.GetKeyDown(KeyCode.R) && anim.GetCurrentAnimatorStateInfo(0).IsName("Sit To Stand") && sjestinav2 == true)
        {
            navAgent.enabled = false;
            anim.SetTrigger("sitagain");
        }
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Sit To Stand") && Input.GetKeyDown(KeyCode.Alpha2))
        {
            navAgent.enabled = true;
            anim.SetTrigger("standingidle");
        }*/
        if (antibiotik == true && !jednom)
        {
            elapsed += Time.deltaTime;
            if (elapsed >= 2f)
            {
                bodovi.GetComponent<EnemyController>().bodovi -= 100;
                scoreText.text = "Bodovi: " + (bodovi.GetComponent<EnemyController>().bodovi).ToString();
                Destroy(gameObject.GetComponent<lastdial>());
                elapsed = 0f;
                anim.SetInteger("seizure", 2);
                AudioManager.Instance.Play(SoundType.Death);
                StartCoroutine(ActivateOnTimer());
                jednom = true;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            following = other.gameObject.transform;
        }
        if (other.CompareTag("object"))
        {
            StartCoroutine(Timer());
        }
        if (other.CompareTag("sjestnavagent1"))
        {
            sjestinav2 = true;
        }
        if (other.CompareTag("sjestinavagent2"))
        {
            sjestinav = true;
        }
        if (other.CompareTag("sjestinavagent3"))
        {
            sjestinav1 = true;
        }
        if (other.CompareTag("sjestinavagent4"))
        {
            sjestinav3 = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            following = null;
        }
        if (other.CompareTag("sjestnavagent1"))
        {
            sjestinav2 = false;
        }
        if (other.CompareTag("sjestinavagent2"))
        {
            sjestinav = false;
        }
        if (other.CompareTag("sjestinavagent3"))
        {
            sjestinav1 = false;
        }
        if (other.CompareTag("sjestinavagent4"))
        {
            sjestinav3 = false;
        }
    }
    void promjenapozicije()
    {
        transform.position = pozicija;
    }
    private IEnumerator ActivateOnTimer()
    {
        yield return new WaitForSeconds(6f);
        promjenapozicije();
        float yRotation = 180 - transform.eulerAngles.y;
        this.transform.Rotate(0f, (360 + yRotation), 0f);
        navAgent.enabled = false;
    }
    private IEnumerator Timer()
    {
        yield return new WaitForSeconds(15f);
        anim.SetInteger("seizure", 1);
        antibiotik = true;
    }
}
