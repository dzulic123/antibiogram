using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stolica : MonoBehaviour
{
    // Start is called before the first frame update
    private Animator anim;
    //private Animator navanim;
    public GameObject character;
    bool sjesti = false;
    void Start()
    {
        //anim = GetComponent<Animator>();
        anim = character.GetComponent<Animator>();
        //navanim = nav1.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (sjesti==true && Input.GetKeyDown(KeyCode.R) && !this.anim.GetCurrentAnimatorStateInfo(0).IsName("Stand To Sit"))
        {
            anim.SetTrigger("sjedni");
            //StartCoroutine(Timer());
            character.GetComponent<MoveBehaviour>().enabled = false;
        }
        if (Input.GetKeyDown(KeyCode.R) && this.anim.GetCurrentAnimatorStateInfo(0).IsName("Stand To Sit"))
        {
            //anim.enabled = true;
            character.GetComponent<MoveBehaviour>().enabled = true;
            anim.SetTrigger("notsitting");
            anim.SetTrigger("idle");
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("sjest"))
        {
            sjesti = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("sjest"))
        {
            sjesti = false;
        }
    }
    /*private IEnumerator Timer()
    {
        yield return new WaitForSeconds(5f);
        anim.enabled = false;
        character.GetComponent <MoveBehaviour>().enabled = false;
    }*/
}
