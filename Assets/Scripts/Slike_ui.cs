using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slike_ui : MonoBehaviour
{
    public GameObject zeroslika1, zeroslika2, zeroslika3, zeroslika4, zeroslika5, zeroslika6, zeroslika7, zeroslika8;
    public GameObject enemy;
    public GameObject slikasbodovima1, slikasbodovima2, slikasbodovima3, slikasbodovima4, slikasbodovima5, slikasbodovima6, slikasbodovima7, slikasbodovima8;
    void Update()
    {
        if(/*enemy.GetComponent<EnemyController>().bodovi <= 0 &&*/ enemy.GetComponent<EnemyController>().bodovi < 100)
        {
            zeroslika1.SetActive(false);
            zeroslika2.SetActive(true);
            zeroslika3.SetActive(true);
            zeroslika4.SetActive(true);
            zeroslika5.SetActive(true);
            zeroslika6.SetActive(true);
            zeroslika7.SetActive(true);
            zeroslika8.SetActive(true);
            slikasbodovima1.SetActive(true);
            slikasbodovima2.SetActive(false);
            slikasbodovima3.SetActive(false);
            slikasbodovima4.SetActive(false);
            slikasbodovima5.SetActive(false);
            slikasbodovima6.SetActive(false);
            slikasbodovima7.SetActive(false);
            slikasbodovima8.SetActive(false);
        }
        if (enemy.GetComponent<EnemyController>().bodovi >= 100 && enemy.GetComponent<EnemyController>().bodovi < 200)
        {
            zeroslika1.SetActive(false);
            zeroslika2.SetActive(false);
            zeroslika3.SetActive(true);
            zeroslika4.SetActive(true);
            zeroslika5.SetActive(true);
            zeroslika6.SetActive(true);
            zeroslika7.SetActive(true);
            zeroslika8.SetActive(true);
            slikasbodovima1.SetActive(true);
            slikasbodovima2.SetActive(true);
            slikasbodovima3.SetActive(false);
            slikasbodovima4.SetActive(false);
            slikasbodovima5.SetActive(false);
            slikasbodovima6.SetActive(false);
            slikasbodovima7.SetActive(false);
            slikasbodovima8.SetActive(false);
        }
        if (enemy.GetComponent<EnemyController>().bodovi >= 200 && enemy.GetComponent<EnemyController>().bodovi < 300)
        {
            zeroslika1.SetActive(false);
            zeroslika2.SetActive(false);
            zeroslika3.SetActive(false);
            zeroslika4.SetActive(true);
            zeroslika5.SetActive(true);
            zeroslika6.SetActive(true);
            zeroslika7.SetActive(true);
            zeroslika8.SetActive(true);
            slikasbodovima1.SetActive(true);
            slikasbodovima2.SetActive(true);
            slikasbodovima3.SetActive(true);
            slikasbodovima4.SetActive(false);
            slikasbodovima5.SetActive(false);
            slikasbodovima6.SetActive(false);
            slikasbodovima7.SetActive(false);
            slikasbodovima8.SetActive(false);
        }
        if (enemy.GetComponent<EnemyController>().bodovi >= 300 && enemy.GetComponent<EnemyController>().bodovi < 400)
        {
            zeroslika1.SetActive(false);
            zeroslika2.SetActive(false);
            zeroslika3.SetActive(false);
            zeroslika4.SetActive(false);
            zeroslika5.SetActive(true);
            zeroslika6.SetActive(true);
            zeroslika7.SetActive(true);
            zeroslika8.SetActive(true);
            slikasbodovima1.SetActive(true);
            slikasbodovima2.SetActive(true);
            slikasbodovima3.SetActive(true);
            slikasbodovima4.SetActive(true);
            slikasbodovima5.SetActive(false);
            slikasbodovima6.SetActive(false);
            slikasbodovima7.SetActive(false);
            slikasbodovima8.SetActive(false);
        }
        if (enemy.GetComponent<EnemyController>().bodovi >= 400 && enemy.GetComponent<EnemyController>().bodovi < 500)
        {
            zeroslika1.SetActive(false);
            zeroslika2.SetActive(false);
            zeroslika3.SetActive(false);
            zeroslika4.SetActive(false);
            zeroslika5.SetActive(false);
            zeroslika6.SetActive(true);
            zeroslika7.SetActive(true);
            zeroslika8.SetActive(true);
            slikasbodovima1.SetActive(true);
            slikasbodovima2.SetActive(true);
            slikasbodovima3.SetActive(true);
            slikasbodovima4.SetActive(true);
            slikasbodovima5.SetActive(true);
            slikasbodovima6.SetActive(false);
            slikasbodovima7.SetActive(false);
            slikasbodovima8.SetActive(false);
        }
        if (enemy.GetComponent<EnemyController>().bodovi >= 500 && enemy.GetComponent<EnemyController>().bodovi <600)
        {
            zeroslika1.SetActive(false);
            zeroslika2.SetActive(false);
            zeroslika3.SetActive(false);
            zeroslika4.SetActive(false);
            zeroslika5.SetActive(false);
            zeroslika6.SetActive(false);
            zeroslika7.SetActive(true);
            zeroslika8.SetActive(true);
            slikasbodovima1.SetActive(true);
            slikasbodovima2.SetActive(true);
            slikasbodovima3.SetActive(true);
            slikasbodovima4.SetActive(true);
            slikasbodovima5.SetActive(true);
            slikasbodovima6.SetActive(true);
            slikasbodovima7.SetActive(false);
            slikasbodovima8.SetActive(false);
        }
        if (enemy.GetComponent<EnemyController>().bodovi >= 600 && enemy.GetComponent<EnemyController>().bodovi < 800)
        {
            zeroslika1.SetActive(false);
            zeroslika2.SetActive(false);
            zeroslika3.SetActive(false);
            zeroslika4.SetActive(false);
            zeroslika5.SetActive(false);
            zeroslika6.SetActive(false);
            zeroslika7.SetActive(false);
            zeroslika8.SetActive(true);
            slikasbodovima1.SetActive(true);
            slikasbodovima2.SetActive(true);
            slikasbodovima3.SetActive(true);
            slikasbodovima4.SetActive(true);
            slikasbodovima5.SetActive(true);
            slikasbodovima6.SetActive(true);
            slikasbodovima7.SetActive(true);
            slikasbodovima8.SetActive(false);
        }
        if (enemy.GetComponent<EnemyController>().bodovi >= 800)
        {
            zeroslika1.SetActive(false);
            zeroslika2.SetActive(false);
            zeroslika3.SetActive(false);
            zeroslika4.SetActive(false);
            zeroslika5.SetActive(false);
            zeroslika6.SetActive(false);
            zeroslika7.SetActive(false);
            zeroslika8.SetActive(false);
            slikasbodovima1.SetActive(true);
            slikasbodovima2.SetActive(true);
            slikasbodovima3.SetActive(true);
            slikasbodovima4.SetActive(true);
            slikasbodovima5.SetActive(true);
            slikasbodovima6.SetActive(true);
            slikasbodovima7.SetActive(true);
            slikasbodovima8.SetActive(true);
        }
    }
}
