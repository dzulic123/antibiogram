using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntibiogramBehaviour : MonoBehaviour
{
    //1. dirne se stapic  SterilniStapic1
    //2. klikne se epruveta Epruveta
    //3. klikne se stapic SterilniStapicUEpruvetu
    //4. klikne se stapic SterilniStapicPetrijeva
    //5. klikne se bocica  Pilula1 
    //6. klikne se pinceta Pinceta1 ide do plamenika
    //7. klikne se pinceta PincetaVadiPilulu
    //8. klikne se Bocica da se vrati BocicaPovratak2
    //9. klikne se druga bocica da dode Pilula2
    //10. klikne se pinceta da vadi drugu pilulu PincetaVadiDruguPilulu
    //11. klikne se druga bocica da se vrati BocicaPovratak

        //ako drugi krug:
        //12. klikne se treca bocica da dode
        //13. klikne se pinceta da vadi trecu pilulu
        //14. klikne se treca bocica da se vrati

        //drugi krug:15. umjesto 12. nadalje 
    //12. klikne se na inkubator - otvaraju se vrata
    //13. klikne se petrijeva - ulazi u inkubator
    //14. klikne se inkubator - zatvaraju se vrata
    //15. klikne se na inkubator - otvaraju se vrata
    //16. klikne se petrijeva - vraca se na stol
    //17. klikne se inkubator - zatvaraju se vrata
    //�ESTITKE

    public int korak = 0;
    public bool drugiKrug;

    public void ResetirajKorake()
    {
        korak = 0;
        drugiKrug = true;
    }

    public int DotaknutaPetrijeva()
    {
        Debug.Log("Antib. drugi krug: " + drugiKrug);

        if (drugiKrug == false)
        {
            if (korak == 12)
            {
                korak++;
                return korak;
            }
            if (korak == 15)
            {
                korak++;
                return korak;
            }
            if (korak == 17)
            {
                korak++;
                drugiKrug = true;
                Debug.Log("drugi krug: " + drugiKrug);
                return korak;
            }
        }
        else
        {
            if (korak == 15)
            {
                korak++;
                Debug.Log("drugi krug: " + drugiKrug);
                return korak;
            }
            if (korak == 18)
            {
                korak++;
                return korak;
            }
            if (korak == 20)
            {
                korak++;
                return korak;
            }
        }
        return 0;
    }
    public int DotaknutaEpruveta()
    {
        if (korak == 1)
        {
            korak++;
            return korak;//da se pokrene animacija
        }
        return 0;
    }
    public int DotaknutInkubator()
    {
        if (drugiKrug == false)
        {
            if (korak == 11)
            {
                korak++;
                return korak;
            }
            if (korak == 13)
            {
                korak++;
                return korak;
            }
            if (korak == 14)
            {
                korak++;
                return korak;
            }
            if (korak == 16)
            {
                korak++;
                return korak;
            }
        }
        else
        {
            if (korak == 14)
            {
                korak++;
                return korak;
            }
            if (korak == 16)
            {
                korak++;
                return korak;
            }
            if (korak == 17)
            {
                korak++;
                return korak;
            }
            if (korak == 19)
            {
                korak++;
                return korak;
            }
        }

        return 0;
    }

    public int DotaknutaBocica1()
    {
        if (korak == 4)
        {
            korak++;
            return korak;//da se pokrene animacija dode bocica
        }
        if (korak == 7)
        {
            korak++;
            return korak;//da se pokrene animacija vraca se bocica
        }
        return 0;
    }

    public int DotaknutaBocica2()
    {
        if (korak == 8)
        {
            korak++;
            return korak;//da se pokrene animacija dode bocica 2
        }
        if (korak == 10)
        {
            korak++;
            return korak;//da se pokrene animacija vraca se bocica 2
        }
        return 0;
    }
    public int DotaknutaBocica3()
    {
        if (korak == 11)
        {
            korak++;
            return korak;//da se pokrene animacija dode bocica 2
        }
        if (korak == 13)
        {
            korak++;
            return korak;//da se pokrene animacija vraca se bocica 2
        }
        return 0;
    }

    public int DotaknutaPinceta()
    {
        if (korak == 5)
        {
            korak++;
            return korak;//da se pokrene animacija ide do plamenika
        }
        if (korak == 6)
        {
            korak++;
            return korak;//da se pokrene animacija vadi pilulu
        }
        if (korak == 9)
        {
            korak++;
            return korak;//da se pokrene animacija vadi pilulu
        }
        if (drugiKrug && korak == 12)
        {
            korak++;
            return korak;//da se pokrene animacija pinceta
        }
        return 0;
    }

    public int DotaknutStapic()
    {
        Debug.Log("stapic:" + korak);
        if (korak == 0)
        {
            korak++;
            return korak;//da se pokrene animacija
        }
        else if(korak == 2)
        {
            korak++;
            return korak;
        }
        else if (korak == 3)
        {
            korak++;
            return korak;
        }
        return 0;
    }

}
