using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PetrijevaAnimacija : MonoBehaviour
{
    Animator a;
    AntibiogramBehaviour antibiogram;
    public GameObject player;
    public GameObject novaPetrijeva;
    public GameObject novaBocica;
    bool drugiKrug;
    public GameObject bodovi;
    public Text scoreText;
    public QuizManager quizGameObject;
    public GameObject notification;
    public GameObject kamera;
    [HideInInspector]
    public bool ulazak = false;
    private bool jednom = false;
    public GameObject invertorygamobj;
    [HideInInspector]
    public bool ulazak1 = false;
    [HideInInspector]
    public int i = 0;
    /*[HideInInspector]
    public bool ulazak3=false;*/

    // Start is called before the first frame update
    void Start()
    {
        a = this.GetComponent<Animator>();
        antibiogram = player.GetComponent<AntibiogramBehaviour>();
        notification.SetActive(false);
    }

    // Update is called once per frame
    void OnMouseOver()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("petrijeva klik");
            int korak = antibiogram.DotaknutaPetrijeva();
            Debug.Log("korak petrijeva: " + korak);
            drugiKrug = antibiogram.drugiKrug;

            if (drugiKrug == false)
            {
                if (korak == 13)
                {
                    a.enabled = true;
                    a.Play("UInkubator");
                }
                if (korak == 16)
                {
                    a.Play("IzInkubatora");
                    bodovi.GetComponent<EnemyController>().bodovi += 100;
                    scoreText.text = "Bodovi: " + (bodovi.GetComponent<EnemyController>().bodovi).ToString();
                    if (!ulazak)
                    {
                        notification.SetActive(true);
                        StartCoroutine(quest());
                    }
                }               
            }
            //drugi krug
            else
            //if(drugiKrug==true && ulazak==true)
            {
                //ostatak prvog kruga, prva petrijeva odlazi na rub stola
                if (korak == 18 )
                {
                    //Debug.Log("aaa");
                    a.Play("Odlazak");
                    antibiogram.ResetirajKorake();
                    return;
                }

                else if (korak == 16 )
                {
                    a.enabled = true;
                    a.Play("UInkubator");
                }
                else if (korak == 19 )
                {
                    a.Play("IzInkubatora");
                    bodovi.GetComponent<EnemyController>().bodovi += 100;
                    scoreText.text = "Bodovi: " + (bodovi.GetComponent<EnemyController>().bodovi).ToString();
                    //i = 2;
                    StartCoroutine(quest1());
                }
                else if (korak == 21 )
                {
                    a.Play("Odlazak");
                }
            }
        }
        //Debug.Log(ulazak);
    }
    /*void Update()
    {
        if (ulazak == true && !jednom)
        {
            //Cursor.lockState = CursorLockMode.None;
            //Time.timeScale = 0f;
            quizGameObject.StartQuiz();
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = 0f;
            kamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
            //Cursor.lockState = CursorLockMode.None;
            /*kamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
            Time.timeScale = 0f;
            invertorygamobj.SetActive(true);*/
            /*Debug.Log("aa");
            jednom = true;
        }
    }*/

    public void UpaliNovuPetrijevu()
    {
        novaPetrijeva.gameObject.SetActive(true);
        novaBocica.gameObject.SetActive(true);
    }
    private IEnumerator quest()
    {
        yield return new WaitForSeconds(9);
        //ulazak = true;
        /*if (!ulazak)
        {
            Destroy(notification);
            ulazak = true;
        }*/
        //Destroy(notification);
        notification.SetActive(false);
        ulazak = true;
        //i = 1;
        //ulazak3 = true;
        //notification.SetActive(false);
        //ulazak = true;
        /*Cursor.lockState = CursorLockMode.None;
        kamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
        Time.timeScale = 0f;
        quizGameObject.StartQuiz();*/
    }
    private IEnumerator quest1()
    {
        yield return new WaitForSeconds(9);
        ulazak1 = true;
    }
    private IEnumerator QuestQuizTimer()
    {
        yield return new WaitForSeconds(10);
        //quest4.SetActive(false);
        /*Cursor.lockState = CursorLockMode.None;
        kamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
        Time.timeScale = 0f;*/
        ulazak = true;
        Cursor.lockState = CursorLockMode.None;
        kamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
        Time.timeScale = 0f;
        quizGameObject.StartQuiz();
        quizGameObject.StartQuiz();
        //Destroy(quest4);
    }
}
