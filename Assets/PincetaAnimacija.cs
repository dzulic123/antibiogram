using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PincetaAnimacija : MonoBehaviour
{
    Animator a;
    AntibiogramBehaviour antibiogram;
    public GameObject player;
    public GameObject petrijeva;
    public GameObject drugaPetrijeva;
    public bool drugiKrug;
    public GameObject bodovi;
    public Text scoreText;

    // Start is called before the first frame update
    void Start()
    {
        a = this.GetComponent<Animator>();
        antibiogram = player.GetComponent<AntibiogramBehaviour>();
    }

    // Update is called once per frame
    void OnMouseOver()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            int korak = antibiogram.DotaknutaPinceta();
            drugiKrug = antibiogram.drugiKrug;
            if (korak == 6)
            {
                a.enabled = true;
                a.Play("Pinceta1");
            }
            if (korak == 7)
            {
                a.Play("PincetaVadiPilulu");
            }
            if (korak == 10)
            {
                a.Play("PincetaVadiDruguPilulu");
            }
            if(drugiKrug == true)
            {
                if(korak == 13)
                {
                    a.Play("PincetaVadiTrecuPilulu");
                }
            }
        }
    }

    void UpaliPilulu1()
    {
        Debug.Log("pali pilulu na petrijevoj");
        drugiKrug = antibiogram.drugiKrug;
        if (drugiKrug == false)
        {
            petrijeva.transform.Find("pillA").gameObject.SetActive(true);
            bodovi.GetComponent<EnemyController>().bodovi += 100;
            scoreText.text = "Bodovi: " + (bodovi.GetComponent<EnemyController>().bodovi).ToString();
        }
        else
        {
            drugaPetrijeva.transform.Find("pillA").gameObject.SetActive(true);
        }
    }
    void UpaliPilulu2()
    {
        drugiKrug = antibiogram.drugiKrug;
        if (drugiKrug == false)
        {
            petrijeva.transform.Find("pillB").gameObject.SetActive(true);
            bodovi.GetComponent<EnemyController>().bodovi += 100;
            scoreText.text = "Bodovi: " + (bodovi.GetComponent<EnemyController>().bodovi).ToString();
        }
        else
        {
            drugaPetrijeva.transform.Find("pillB").gameObject.SetActive(true);
        }
    }
    void UpaliPilulu3()
    {
        drugiKrug = antibiogram.drugiKrug;
        if (drugiKrug == false)
        {
            petrijeva.transform.Find("pillC").gameObject.SetActive(true);
        }
        else
        {
            drugaPetrijeva.transform.Find("pillC").gameObject.SetActive(true);
        }
    }
}
