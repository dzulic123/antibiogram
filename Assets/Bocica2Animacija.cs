using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bocica2Animacija : MonoBehaviour
{
    Animator a;
    AntibiogramBehaviour antibiogram;
    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        a = this.GetComponent<Animator>();
        antibiogram = player.GetComponent<AntibiogramBehaviour>();
    }

    // Update is called once per frame
    void OnMouseOver()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            int korak = antibiogram.DotaknutaBocica2();
            Debug.Log("bocica2:" + korak);
            if (korak == 9)
            {
                a.enabled = true;
                a.Play("Pilula2");
            }
            if (korak == 11)
            {
                a.Play("BocicaPovratak");
            }

        }
    }
}
