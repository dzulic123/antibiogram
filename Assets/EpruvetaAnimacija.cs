using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EpruvetaAnimacija : MonoBehaviour
{
    Animator a;
    AntibiogramBehaviour antibiogram;
    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        a = this.GetComponent<Animator>();
        antibiogram = player.GetComponent<AntibiogramBehaviour>();
    }

    public void SpremiEpruvetu()
    {
        a.Play("EpruvetaZatvaranje");
    }

    // Update is called once per frame
    void OnMouseOver()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            int korak = antibiogram.DotaknutaEpruveta();
            if (korak != 0)
            {
                a.enabled = true;
                if (korak == 2)
                {
                    a.Play("Epruveta");
                }
                else if(korak == 10)
                {

                }
            }

        }
    }
}
