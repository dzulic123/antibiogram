using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    private Slider slider;

    private TextMeshProUGUI sljedeciLvlText;
    private TextMeshProUGUI trenutniLvlText;


    public GameObject trenutniLvl;
    public GameObject sljedeciLvl;
    public float FillSpeed = 0.2f;
    private float targetProgress = 0;

    private int trenutniLevel=1;


    private void Awake()
    {
        slider = gameObject.GetComponent<Slider>();

        sljedeciLvlText = sljedeciLvl.GetComponent<TextMeshProUGUI>();
        trenutniLvlText = trenutniLvl.GetComponent<TextMeshProUGUI>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //IncrementProgress(0.75f);
    }

    // Update is called once per frame
    void Update()
    {
        //if (slider.value < targetProgress)
         //   slider.value += FillSpeed * Time.deltaTime;
    }

    public void IncrementProgress() //0.2
    {
        slider.value += FillSpeed;
        if (slider.value == slider.maxValue)
        {
            trenutniLevel++;
            trenutniLvlText.text = trenutniLevel.ToString();
            sljedeciLvlText.text = (trenutniLevel + 1).ToString();

            slider.value = slider.minValue;
        }
    }


}
