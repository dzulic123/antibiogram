﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class Instrukcije : MonoBehaviour
{
    public Canvas Canvas;
    public GameObject trenutna;
    public GameObject prethodna;
    public GameObject sljedeca;
    private int radnja = 0;
    private bool drugiKrug = false;

    public ProgressBar ProgressBar;
    public GameObject Srce;

    private TextMeshProUGUI prethodniText;
    private TextMeshProUGUI trenutniText;
    private TextMeshProUGUI sljedeciText;



    public void Awake()
    {
        prethodniText = prethodna.GetComponent<TextMeshProUGUI>();
        trenutniText = trenutna.GetComponent<TextMeshProUGUI>();
        sljedeciText = sljedeca.GetComponent<TextMeshProUGUI>();
    }

    List<string> koraci = new List<string> { "Klikni sterilni štapić", "Klikni na epruvetu(posuda s epruvatama)", "Klikni sterilni štapić(da ode u epruvetu)",
        "Klikni na sterilni štapić da se bakterija razmaže po petrijevoj zdjelici", "Klikni na lijevu boćicu", "Klikni na pincetu kako bi otišla do plamenika", "Klikni na pincetu kako bi izvadio pilulu iz bočice i prenijela na petrijevu zdjelicu"
    , "Klikni na bočicu da se vrati na mjesto", "Klikni na drugu bočicu da dođe", "Klikni na pincetu da vadi drugu pilulu iz druge bočice", "Klikni na drugu bočicu da se vrati na mjesto", "Klikne se na inkubator - otvaraju se vrata", "Klikne se petrijeva - ulazi u inkubator"
    , "Klikni na inkubator - zatvaraju se vrata", "Klikni na inkubator - otvaraju se vrata", "Klikni na petrijevu zdjelicu - vraća se na stol", "Klikni na inkubator - zatvaraju se vrata"};

    List<string> koraci2 = new List<string> { "Klikni sterilni štapić", "Klikni na epruvetu(posuda s epruvatama)", "Klikni sterilni štapić(da ode u epruvetu)",
        "Klikni na sterilni štapić da se bakterija razmaže po petrijevoj zdjelici", "Klikni na lijevu boćicu", "Klikni na pincetu kako bi otišla do plamenika", "Klikni na pincetu kako bi izvadio pilulu iz bočice i prenijela na petrijevu zdjelicu"
    , "Klikni na bočicu da se vrati na mjesto", "Klikni na drugu bočicu da dođe", "Klikni na pincetu da vadi drugu pilulu iz druge bočice", "Klikni na drugu bočicu da se vrati na mjesto", "Klikni na treću bočicu", "Klikni na pincetu da se izvadi treća pilula",  
       "Klikni na inkubator - otvaraju se vrata", "Klikni na petrijevu zdjelicu - vraća se na stol", "Klikni na inkubator - zatvaraju se vrata", };
    
    public void PostaviTrenutnuRadnju()
    {
        //koraci iz liste koraci
        if (drugiKrug == false)
        {
            if (radnja == 0)
            {
                prethodniText.text = "Prethodna: ";
            }
            else
            {
                prethodniText.text = "Prethodna: " + koraci[radnja - 1];
            }
            //zadnja radnja
            if (radnja == koraci.Count) //-1
            {
                //dobiju se srca, ide drugi krug, radnja ide od 0
                Srce.GetComponent<TextMeshProUGUI>().text = "50";
                drugiKrug = true;
                radnja = 0;
            }
            sljedeciText.text = "Sljedeća: " + koraci[radnja + 1];

            trenutniText.text = koraci[radnja];           
        }
        //koraci iz liste koraci2, drugi krug
        else
        {
            if (radnja == 0)
            {
                prethodniText.text = "Prethodna: ";
            }
            else
            {
                prethodniText.text = "Prethodna: " + koraci2[radnja - 1];
            }
            //zadnja radnja
            if (radnja == koraci2.Count) //-1
            {
                Srce.GetComponent<TextMeshProUGUI>().text = "100";
                drugiKrug = true;
            }
            sljedeciText.text = "Sljedeća: " + koraci2[radnja + 1];

            trenutniText.text = koraci2[radnja];
        }

        trenutniText.fontStyle = FontStyles.Bold;
        Color color = new Color();
        ColorUtility.TryParseHtmlString("#ff112233", out color);
        trenutniText.color = color;
        radnja++;
        ProgressBar.IncrementProgress();


    }
}
