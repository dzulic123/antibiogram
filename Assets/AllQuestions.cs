﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllQuestions 
{
    public List<QuestionsAndAnswers> questionAndAnswer = new List<QuestionsAndAnswers>();

    public AllQuestions()
    {
        // pitanja za prvi kviz
        QuestionsAndAnswers qa = new QuestionsAndAnswers();
        qa.Question = "Što se prvo uzima u izradi antibiograma?";
        qa.Answers = new string[4] { "sterilni štapić i epruveta s bakterijama", "sterilni štapić i plamenik", "sterilni štapić i Petrijeva zdjelica", "plamenik i epruveta s bakterijama"  };
        qa.correctAnswer = 1;

        QuestionsAndAnswers qa2 = new QuestionsAndAnswers();
        qa2.Question = "S čime se uzima antibiotik?";
        qa2.Answers = new string[4] { "sterilnim štapićem", "rukom", "posudom", "pincetom" };
        qa2.correctAnswer = 4;

        QuestionsAndAnswers qa3 = new QuestionsAndAnswers();
        qa3.Question = "Koji je antibiotik najbolje reagirao?";
        qa3.Answers = new string[4] { "prvi", "drugi", "podjednako", "niti jedan" };
        qa3.correctAnswer = 2;

        QuestionsAndAnswers qa4 = new QuestionsAndAnswers();
        qa4.Question = "Što se radi s Petrijevom zdjelicom nakon inkubatora?";
        qa4.Answers = new string[4] { "stavlja se na stol i mjeri ravnalom", "mjeri se ravnalnom", "stavlja se na stol", "čeka se reakcija" };
        qa4.correctAnswer = 1;

        QuestionsAndAnswers qa5 = new QuestionsAndAnswers();
        qa5.Question = "Koliko vremena treba Petrijeva zdjelica biti u inkubatoru?";
        qa5.Answers = new string[4] { "par sati", "5-7h", "10-12h", "više od 14 sati" };
        qa5.correctAnswer = 4;

        // pitanja za drugi kviz
        QuestionsAndAnswers qa6 = new QuestionsAndAnswers();
        qa6.Question = "Što se prvo uzima u izradi antibiograma?";
        qa6.Answers = new string[4] { "Petrijeva zdjelica", "sterilni štapić", "plamenik", "epruveta s bakterijama" };
        qa6.correctAnswer = 2;

        QuestionsAndAnswers qa7 = new QuestionsAndAnswers();
        qa7.Question = "Iz čega se vadi antibiotik?";
        qa7.Answers = new string[4] { "epruvete", "posude", "sterilne bočice", "sterilne zdjelice" };
        qa7.correctAnswer = 3;

        QuestionsAndAnswers qa8 = new QuestionsAndAnswers();
        qa8.Question = "Antibiotik je najbolje reagirao tamo gdje je promjer zone inhibicije ... ?";
        qa8.Answers = new string[4] { "najmanji", "najtamniji", "najveći", "najsvjetliji" };
        qa8.correctAnswer = 3;

        QuestionsAndAnswers qa9 = new QuestionsAndAnswers();
        qa9.Question = "Koji je antibiotik najbolje reagirao?";
        qa9.Answers = new string[4] { "prvi", "drugi", "treći", "četvrti" };
        qa9.correctAnswer = 1;


        QuestionsAndAnswers qa10 = new QuestionsAndAnswers();
        qa10.Question = "Čemu služi Petrijeva zdjelica?";
        qa10.Answers = new string[4] { "za antibiotik", "za promatranje raznih kultura bakterija", "za spremanje sterilnih štapića", "za premazivanje površine" };
        qa10.correctAnswer = 2;

        
        questionAndAnswer.Add(qa);
        questionAndAnswer.Add(qa2);
        questionAndAnswer.Add(qa3);
        questionAndAnswer.Add(qa4); 
        questionAndAnswer.Add(qa5);

        
        questionAndAnswer.Add(qa6);
        questionAndAnswer.Add(qa7);
        questionAndAnswer.Add(qa8);
        questionAndAnswer.Add(qa9);
        questionAndAnswer.Add(qa10);
        
    }


}
